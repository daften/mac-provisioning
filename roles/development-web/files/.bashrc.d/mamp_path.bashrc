#!/usr/bin/env bash

# Add the general MAMP binary folder to the path
export PATH="/Applications/MAMP/Library/bin:$PATH"